<?php

declare(strict_types = 1);

/**
 * Copyright (C) 2021 PRONOVIX GROUP.
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301,
 * USA.
 */

namespace Drupal\view_usernames;

use Drupal\Core\Access\AccessResult;
use Drupal\Core\Access\AccessResultInterface;
use Drupal\Core\DependencyInjection\ContainerInjectionInterface;
use Drupal\Core\Field\FieldDefinitionInterface;
use Drupal\Core\Field\FieldItemListInterface;
use Drupal\Core\Session\AccountInterface;
use Drupal\user\UserInterface;
use Drupal\view_usernames\Contracts\ViewUsernameAccessDeciderInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Handles username access on the entity and field level.
 *
 * @internal This class is not part of the module's public programming API.
 */
final class EntityHooks implements ContainerInjectionInterface {

  /**
   * The view username access decider.
   *
   * @var \Drupal\view_usernames\Contracts\ViewUsernameAccessDeciderInterface
   */
  private ViewUsernameAccessDeciderInterface $decider;

  /**
   * Constructs a new object.
   *
   * @param \Drupal\view_usernames\Contracts\ViewUsernameAccessDeciderInterface $decider
   *   The view username access decider.
   */
  public function __construct(ViewUsernameAccessDeciderInterface $decider) {
    $this->decider = $decider;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container): self {
    return new self($container->get('view_usernames.view_username_access_decider'));
  }

  /**
   * Handles user entity access checking.
   *
   * @param \Drupal\user\UserInterface $entity
   *   The entity to check access to.
   * @param string $operation
   *   The operation that is to be performed on $entity. Usually one of:
   *   - "view"
   *   - "update"
   *   - "delete".
   * @param \Drupal\Core\Session\AccountInterface $account
   *   The account trying to access the entity.
   *
   * @return \Drupal\Core\Access\AccessResultInterface
   *   The access result. hook_entity_access() has detailed documentation.
   *
   * @see \hook_ENTITY_TYPE_access()
   */
  public function userEntityAccess(UserInterface $entity, string $operation, AccountInterface $account): AccessResultInterface {
    if ($operation !== 'view label') {
      return AccessResult::neutral();
    }

    return $this->decider->canViewUserName($account, $entity);
  }

  /**
   * Handles field access checking.
   *
   * @param string $operation
   *   The operation to be performed. See
   *   \Drupal\Core\Entity\EntityAccessControlHandlerInterface::fieldAccess()
   *   for possible values.
   * @param \Drupal\Core\Field\FieldDefinitionInterface $field_definition
   *   The field definition.
   * @param \Drupal\Core\Session\AccountInterface $account
   *   The user account to check.
   * @param \Drupal\Core\Field\FieldItemListInterface|null $items
   *   (optional) The entity field object for which to check access, or NULL if
   *   access is checked for the field definition, without any specific value
   *   available. Defaults to NULL.
   *
   * @return \Drupal\Core\Access\AccessResultInterface
   *   The access result.
   *
   * @phpstan-param \Drupal\Core\Field\FieldItemListInterface<\Drupal\Core\Field\FieldItemInterface>|null $items
   *
   * @see \hook_entity_field_access()
   */
  public function entityFieldAccess(string $operation, FieldDefinitionInterface $field_definition, AccountInterface $account, ?FieldItemListInterface $items): AccessResultInterface {
    // When $items->getEntity()->id() === NULL then it is a "stub user".
    // e.g, this method called meanwhile user creation.
    if ($operation !== 'view' || $items === NULL || !$items->getEntity() instanceof UserInterface || $items->getEntity()->id() === NULL || $field_definition->getName() !== 'name') {
      return AccessResult::neutral();
    }

    return $this->decider->canViewUserName($account, $items->getEntity());
  }

}
