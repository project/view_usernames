<?php

declare(strict_types = 1);

/**
 * Copyright (C) 2021 PRONOVIX GROUP.
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301,
 * USA.
 */

namespace Drupal\view_usernames;

use Drupal\Core\Cache\CacheableDependencyInterface;
use Drupal\view_usernames\Contracts\ConfigurableUserFormatNameHardeningBypasserInterface;
use Drupal\view_usernames\Type\Bypasser\BypassIt;
use Drupal\view_usernames\Type\Bypasser\BypasserResult;
use Drupal\view_usernames\Type\Bypasser\ExecuteIt;

/**
 * Bypasser that can temporarily disable the hardening in the current request.
 *
 * @internal This class is not part of the module's public programming API.
 */
final class TemporaryUserFormatNameHardeningBypasser implements ConfigurableUserFormatNameHardeningBypasserInterface {

  /**
   * The current state.
   *
   * @var \Drupal\view_usernames\Type\Bypasser\BypasserResult
   *
   * @phpcs:disable SlevomatCodingStandard.Variables.UnusedVariable.UnusedVariable
   */
  private BypasserResult $state;

  /**
   * Constructs a new object.
   */
  public function __construct() {
    $this->state = ExecuteIt::create();
  }

  /**
   * {@inheritdoc}
   */
  public function activate(?CacheableDependencyInterface $cacheability = NULL): void {
    $this->state = BypassIt::create();
    if ($cacheability) {
      $this->state = $this->state->withCacheability($cacheability);
    }
  }

  /**
   * {@inheritdoc}
   */
  public function deactivate(?CacheableDependencyInterface $cacheability = NULL): void {
    $this->state = ExecuteIt::create();
    if ($cacheability) {
      $this->state = $this->state->withCacheability($cacheability);
    }
  }

  /**
   * {@inheritdoc}
   */
  public function shouldByPass(): BypasserResult {
    return $this->state;
  }

}
