<?php

declare(strict_types = 1);

/**
 * Copyright (C) 2022 PRONOVIX GROUP.
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301,
 * USA.
 */

namespace Drupal\view_usernames\Utility;

use Drupal\Core\Cache\CacheableDependencyInterface;
use Drupal\Core\Cache\CacheableMetadata;
use Drupal\Core\Render\RendererInterface;
use Symfony\Component\HttpFoundation\RequestStack;

/**
 * Utility command that helps to bubble up cacheability information if possible.
 *
 * @internal This class is not part of the module's public programming API.
 */
final class CacheabilityBubbleUpper {

  /**
   * The request stack.
   *
   * @var \Symfony\Component\HttpFoundation\RequestStack
   */
  private RequestStack $requestStack;

  /**
   * The renderer.
   *
   * @var \Drupal\Core\Render\RendererInterface
   */
  private RendererInterface $renderer;

  /**
   * Constructs a new object.
   *
   * @param \Symfony\Component\HttpFoundation\RequestStack $request_stack
   *   The request stack.
   * @param \Drupal\Core\Render\RendererInterface $renderer
   *   The renderer.
   */
  public function __construct(RequestStack $request_stack, RendererInterface $renderer) {
    $this->requestStack = $request_stack;
    $this->renderer = $renderer;
  }

  /**
   * Creates a new instance with defaults.
   *
   * @return static
   */
  public static function createWithDefaults(): self {
    return new self(\Drupal::service('request_stack'), \Drupal::service('renderer'));
  }

  /**
   * Bubbles up cacheability information is possible.
   *
   * @param \Drupal\Core\Cache\CacheableDependencyInterface $cacheability
   *   The cacheability information to bubble up.
   */
  public function bubbleUpIfPossible(CacheableDependencyInterface $cacheability): void {
    $current_request = $this->requestStack->getCurrentRequest();
    if ($current_request && $current_request->isMethodCacheable() && $this->renderer->hasRenderContext()) {
      $build = [];
      CacheableMetadata::createFromRenderArray($build)->addCacheableDependency($cacheability)->applyTo($build);
      $this->renderer->render($build);
    }
  }

}
