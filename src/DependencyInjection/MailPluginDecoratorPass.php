<?php

declare(strict_types = 1);

/**
 * Copyright (C) 2022 PRONOVIX GROUP.
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301,
 * USA.
 */

namespace Drupal\view_usernames\DependencyInjection;

use Drupal\view_usernames\Mail\MailManagerDecorator;
use Symfony\Component\DependencyInjection\Compiler\CompilerPassInterface;
use Symfony\Component\DependencyInjection\ContainerBuilder;
use Symfony\Component\DependencyInjection\Reference;

/**
 * Registers the Mail plugin decorator.
 *
 * @internal This class is not part of the module's public programming API.
 */
final class MailPluginDecoratorPass implements CompilerPassInterface {

  /**
   * {@inheritdoc}
   */
  public function process(ContainerBuilder $container): void {
    // Decorating a plugin manager is not easy.
    // @see https://www.drupal.org/project/drupal/issues/2958184
    // But not impossible.
    // @see \Drupal\webprofiler\Compiler\DecoratorPass
    $definition = $container->findDefinition('plugin.manager.mail');
    $definition->setPublic(FALSE);
    $container->setDefinition('view_usernames.plugin.manager.mail.default', $definition);
    $container->register('plugin.manager.mail', MailManagerDecorator::class)
      ->addArgument(new Reference('container.namespaces'))
      ->addArgument(new Reference('cache.discovery'))
      ->addArgument(new Reference('module_handler'))
      ->addArgument(new Reference('view_usernames.plugin.manager.mail.default'))
      ->addArgument(new Reference('view_usernames.user_format_name_hardening_bypasser'));
  }

}
