<?php

declare(strict_types = 1);

/**
 * Copyright (C) 2022 PRONOVIX GROUP.
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301,
 * USA.
 */

namespace Drupal\view_usernames\Cache;

use Drupal\Core\Cache\CacheableDependencyInterface;

/**
 * Immutable cacheability information storage.
 *
 * (That for some reason, does not exist in Drupal core.)
 *
 * @internal This class is not part of the module's public programming API.
 */
final class ImmutableCacheableMetadata implements CacheableDependencyInterface {

  /**
   * Array of cache context.
   *
   * @var string[]
   */
  private array $contexts;

  /**
   * Array of cache tags.
   *
   * @var string[]
   */
  private array $tags;

  /**
   * The cache max age.
   *
   * @var int
   */
  private int $maxAge;

  /**
   * Constructs a new object.
   *
   * @param string[] $contexts
   *   Array of cache context.
   * @param string[] $tags
   *   Array of cache tags.
   * @param int $max_age
   *   The cache max age.
   */
  public function __construct(array $contexts, array $tags, int $max_age) {
    $this->contexts = $contexts;
    $this->tags = $tags;
    // @todo Nice to have, validate with an assert library that $max_age >= -1.
    $this->maxAge = $max_age;
  }

  /**
   * {@inheritdoc}
   */
  public function getCacheContexts(): array {
    return $this->contexts;
  }

  /**
   * {@inheritdoc}
   */
  public function getCacheTags(): array {
    return $this->tags;
  }

  /**
   * {@inheritdoc}
   */
  public function getCacheMaxAge(): int {
    return $this->maxAge;
  }

}
