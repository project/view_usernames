<?php

declare(strict_types = 1);

/**
 * Copyright (C) 2021-2022 PRONOVIX GROUP.
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301,
 * USA.
 */

namespace Drupal\view_usernames\Mail;

use Drupal\Core\Annotation\Mail;
use Drupal\Core\Cache\CacheBackendInterface;
use Drupal\Core\Extension\ModuleHandlerInterface;
use Drupal\Core\Mail\MailInterface;
use Drupal\Core\Mail\MailManagerInterface;
use Drupal\Core\Plugin\DefaultPluginManager;
use Drupal\view_usernames\Cache\ImmutableCacheableMetadata;
use Drupal\view_usernames\Contracts\ConfigurableUserFormatNameHardeningBypasserInterface;
use Drupal\view_usernames\Contracts\UserFormatNameHardeningBypasserInterface;

/**
 * Decorates the mail manager.
 *
 * Ensures that view_usernames_user_format_name_alter() is bypassed when emails
 * are sent therefore [xy:display-name] (where xy is a user entity reference)
 * resolves. Why? Because when an email is sent the acting user's permissions
 * must be ignored. Even an anonymous user can trigger an email that is sent to
 * a registered user and the email contains the registered user's username.
 *
 * @see https://project.pronovix.net/issues/15121#note-6
 *
 * @internal This class is not part of the module's public programming API.
 */
final class MailManagerDecorator extends DefaultPluginManager implements MailManagerInterface {

  /**
   * The view_usernames_user_format_name_alter() bypass decider.
   *
   * @var \Drupal\view_usernames\Contracts\UserFormatNameHardeningBypasserInterface
   */
  private UserFormatNameHardeningBypasserInterface $bypasser;

  /**
   * The decorated mail manager.
   *
   * @var \Drupal\Core\Mail\MailManagerInterface
   */
  private MailManagerInterface $decorated;

  /**
   * Constructs a new object.
   *
   * @param \Traversable $namespaces
   *   An object that implements \Traversable which contains the root paths
   *   keyed by the corresponding namespace to look for plugin implementations.
   * @param \Drupal\Core\Cache\CacheBackendInterface $cache_backend
   *   Cache backend instance to use.
   * @param \Drupal\Core\Extension\ModuleHandlerInterface $module_handler
   *   The module handler.
   * @param \Drupal\Core\Mail\MailManagerInterface $mail_manager
   *   The decorated mail manager.
   * @param \Drupal\view_usernames\Contracts\UserFormatNameHardeningBypasserInterface $bypasser
   *   The view_usernames_user_format_name_alter() bypass decider.
   */
  public function __construct(\Traversable $namespaces, CacheBackendInterface $cache_backend, ModuleHandlerInterface $module_handler, MailManagerInterface $mail_manager, UserFormatNameHardeningBypasserInterface $bypasser) {
    parent::__construct('Plugin/Mail', $namespaces, $module_handler, MailInterface::class, Mail::class);
    $this->alterInfo('mail_backend_info');
    $this->setCacheBackend($cache_backend, 'mail_backend_plugins');
    $this->decorated = $mail_manager;
    $this->bypasser = $bypasser;
  }

  /**
   * {@inheritdoc}
   */
  public function mail($module, $key, $to, $langcode, $params = [], $reply = NULL, $send = TRUE): array {
    if ($this->bypasser instanceof ConfigurableUserFormatNameHardeningBypasserInterface) {
      // As of today, email rendering is executed in a dedicated render
      // context, although we would like to be sure that bypassing here
      // do not have any side effects in the "top" execution context therefore
      // we mark the render result uncacheable - which should only bubble up
      // to the email render's own render context and not above that.
      // @see \Drupal\Core\Mail\MailManager::mail()
      $this->bypasser->activate(new ImmutableCacheableMetadata([], [], 0));
    }

    $result = $this->decorated->mail($module, $key, $to, $langcode, $params, $reply, $send);

    if ($this->bypasser instanceof ConfigurableUserFormatNameHardeningBypasserInterface) {
      $this->bypasser->deactivate();
    }

    return $result;
  }

  /**
   * {@inheritdoc}
   */
  public function getInstance(array $options): object|false {
    return $this->decorated->getInstance($options);
  }

}
