<?php

declare(strict_types = 1);

/**
 * Copyright (C) 2022 PRONOVIX GROUP.
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301,
 * USA.
 */

namespace Drupal\view_usernames\EventSubscriber;

use Drupal\Core\DependencyInjection\ClassResolverInterface;
use Drupal\jsonapi\Controller\EntityResource;
use Drupal\view_usernames\Contracts\ConfigurableUserFormatNameHardeningBypasserInterface;
use Drupal\view_usernames\Contracts\UserFormatNameHardeningBypasserInterface;
use Symfony\Component\EventDispatcher\EventSubscriberInterface;
use Symfony\Component\HttpKernel\Event\ControllerEvent;
use Symfony\Component\HttpKernel\Event\RequestEvent;
use Symfony\Component\HttpKernel\KernelEvents;

/**
 * Bypasses view_usernames_user_format_name_alter() hardening on JSON API calls.
 *
 * JSON API does proper access checking therefore the extra mitigation is
 * unnecessary (that causes early rendering issue and HTTP 500).
 *
 * @internal This class is not part of the module's public programming API.
 */
final class JsonApiEarlyRenderingFixEventSubscriber implements EventSubscriberInterface {

  /**
   * The class resolver.
   *
   * @var \Drupal\Core\DependencyInjection\ClassResolverInterface
   */
  private ClassResolverInterface $classResolver;

  /**
   * User format name bypasser.
   *
   * @var \Drupal\view_usernames\Contracts\UserFormatNameHardeningBypasserInterface
   */
  private UserFormatNameHardeningBypasserInterface $bypasser;

  /**
   * Constructs a new object.
   *
   * @param \Drupal\Core\DependencyInjection\ClassResolverInterface $class_resolver
   *   The class resolver.
   * @param \Drupal\view_usernames\Contracts\UserFormatNameHardeningBypasserInterface $bypasser
   *   User format name bypasser.
   */
  public function __construct(ClassResolverInterface $class_resolver, UserFormatNameHardeningBypasserInterface $bypasser) {
    $this->classResolver = $class_resolver;
    $this->bypasser = $bypasser;
  }

  /**
   * Reacts to controller events.
   *
   * @param \Symfony\Component\HttpKernel\Event\ControllerEvent $event
   *   The controller event.
   */
  public function onController(ControllerEvent $event): void {
    $callable = $event->getController();

    $is_entity_resource_controller = FALSE;
    // @see https://www.php.net/manual/en/language.types.callable.php#language.types.callable
    if (is_array($callable)) {
      if (is_object($callable[0])) {
        $is_entity_resource_controller = $callable[0] instanceof EntityResource;
      }
      else {
        // The controller also registered as a service: jsonapi.entity_resource.
        $is_entity_resource_controller = $callable[0] === EntityResource::class || $this->classResolver->getInstanceFromDefinition($callable[0]) instanceof EntityResource;
      }
    }
    elseif (is_object($callable)) {
      $is_entity_resource_controller = $callable instanceof EntityResource;
    }
    elseif (is_string($callable) && mb_strpos($callable, '::') !== FALSE) {
      [$class] = explode('::', $callable);
      $is_entity_resource_controller = $class === EntityResource::class;
    }
    // Otherwise, we fail silently for because we could not extract
    // information about the controller (eg.: from a closure).
    if ($is_entity_resource_controller && $this->bypasser instanceof ConfigurableUserFormatNameHardeningBypasserInterface) {
      // If we bubbled up anything here, we would be back to square one.
      $this->bypasser->activate();
    }
  }

  /**
   * Ensures the bypass flag resets on every (sub)request.
   *
   * @param \Symfony\Component\HttpKernel\Event\RequestEvent $event
   *   The request event.
   */
  public function onRequest(RequestEvent $event): void {
    if ($this->bypasser instanceof ConfigurableUserFormatNameHardeningBypasserInterface) {
      $this->bypasser->deactivate();
    }
  }

  /**
   * {@inheritdoc}
   */
  public static function getSubscribedEvents(): array {
    $events[KernelEvents::REQUEST][] = ['onRequest'];
    // Make sure it runs before early_rendering_controller_wrapper_subscriber.
    $events[KernelEvents::CONTROLLER][] = ['onController', 16];
    return $events;
  }

}
