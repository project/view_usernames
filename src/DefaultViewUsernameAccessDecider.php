<?php

declare(strict_types = 1);

/**
 * Copyright (C) 2021 - 2023 PRONOVIX GROUP.
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301,
 * USA.
 */

namespace Drupal\view_usernames;

use Drupal\Core\Access\AccessResult;
use Drupal\Core\Access\AccessResultAllowed;
use Drupal\Core\Access\AccessResultForbidden;
use Drupal\Core\Session\AccountInterface;
use Drupal\user\UserInterface;
use Drupal\view_usernames\Contracts\ViewUsernameAccessDeciderInterface;

/**
 * Default view username access decider.
 *
 * @internal This class is not part of the module's public programming API.
 */
final class DefaultViewUsernameAccessDecider implements ViewUsernameAccessDeciderInterface {

  /**
   * {@inheritdoc}
   */
  public function canViewUserName(AccountInterface $acting_user, UserInterface $other_user): AccessResultAllowed|AccessResultForbidden {
    // Username of anonymous is always visible to everyone.
    if ($other_user->isAnonymous()) {
      return AccessResult::allowed();
    }

    // Users can always see their own usernames.
    if ((int) $other_user->id() === (int) $acting_user->id()) {
      return AccessResult::allowed();
    }

    // @see \Drupal\Core\Cache\Context\AccountPermissionsCacheContext::getCacheableMetadata()
    // The permissions hash changes when:
    // - a user is updated to have different roles;
    $cache_tags_by_acting_user = ['user:' . $acting_user->id()];
    // - a role is updated to have different permissions.
    foreach ($acting_user->getRoles() as $rid) {
      $cache_tags_by_acting_user[] = sprintf('config:user.role.%s', $rid);
    }

    // Users with the first permission can always access.
    // Users with the second permission can always see other user's username.
    if ($acting_user->hasPermission('administer users') || $acting_user->hasPermission('view usernames')) {
      return AccessResult::allowed()->addCacheTags($cache_tags_by_acting_user);
    }

    // Returning forbidden here is our safest option. Entity- and field access
    // works differently. Field access is 99% of times allowed by default,
    // in contradiction, entity access is neutral which leads to access denied
    // if no access implementation granted explicit access.
    // @see \Drupal\Core\Entity\EntityAccessControlHandler::checkAccess()
    // @see \Drupal\Core\Entity\EntityAccessControlHandler::fieldAccess()
    // @see \Drupal\Core\Field\FieldItemList::defaultAccess()
    // Although, we would like allow other modules to extend the current access
    // rules with their own, and service decoration is the answer for that.
    // A few real life-ish examples when a user's name can be exposed on a site:
    // - a user is internal and can create content on a site where content
    // authoring information is exposed,
    // - users checked a checkbox somewhere and with that accepted that their
    // usernames can be exposed.
    // The above used cache dependencies must be applied on this to ensure
    // proper cache invalidation.
    return AccessResult::forbidden(sprintf('View usernames module revoked access. See: \%s()', __METHOD__))->addCacheTags($cache_tags_by_acting_user);
  }

}
