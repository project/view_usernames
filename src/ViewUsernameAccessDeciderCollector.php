<?php

declare(strict_types = 1);

/**
 * Copyright (C) 2023 PRONOVIX GROUP.
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301,
 * USA.
 */

namespace Drupal\view_usernames;

use Drupal\Core\Access\AccessResult;
use Drupal\Core\Access\AccessResultAllowed;
use Drupal\Core\Access\AccessResultForbidden;
use Drupal\Core\Cache\Cache;
use Drupal\Core\DependencyInjection\ClassResolverInterface;
use Drupal\Core\Session\AccountInterface;
use Drupal\user\UserInterface;
use Drupal\view_usernames\Contracts\ViewUsernameAccessDeciderInterface;

/**
 * Collects view username access deciders.
 *
 * @internal This class is not part of the module's public programming API.
 */
final class ViewUsernameAccessDeciderCollector implements ViewUsernameAccessDeciderInterface {

  /**
   * The list of decider service ids.
   *
   * @var string[]
   */
  private array $deciderIds;

  /**
   * The class resolver.
   *
   * @var \Drupal\Core\DependencyInjection\ClassResolverInterface
   */
  private ClassResolverInterface $classResolver;

  /**
   * Constructs a new object.
   *
   * @param \Drupal\Core\DependencyInjection\ClassResolverInterface $class_resolver
   *   The class resolver.
   * @param string[] $decider_ids
   *   The list of decider service ids.
   */
  public function __construct(ClassResolverInterface $class_resolver, array $decider_ids) {
    assert(!empty($decider_ids), 'The list of decider service IDs MUST NOT be empty.');
    $this->deciderIds = $decider_ids;
    $this->classResolver = $class_resolver;
  }

  /**
   * {@inheritdoc}
   */
  public function canViewUserName(AccountInterface $acting_user, UserInterface $other_user): AccessResultAllowed|AccessResultForbidden {
    $forbidden_cache_contexts = [];
    $forbidden_cache_tags = [];
    $forbidden_cache_ages = [];

    foreach ($this->deciderIds as $decider_id) {
      /** @var \Drupal\view_usernames\Contracts\ViewUsernameAccessDeciderInterface $decider */
      $decider = $this->classResolver->getInstanceFromDefinition($decider_id);
      $decision = $decider->canViewUserName($acting_user, $other_user);
      if ($decision->isAllowed()) {
        return $decision;
      }

      $forbidden_cache_contexts[] = $decision->getCacheContexts();
      $forbidden_cache_tags[] = $decision->getCacheTags();
      $forbidden_cache_ages[] = $decision->getCacheMaxAge();
    }

    return AccessResult::forbidden('No deciders granted access.')
      ->addCacheContexts(array_merge(...$forbidden_cache_contexts))
      ->addCacheTags(array_merge(...$forbidden_cache_tags))
      ->setCacheMaxAge(Cache::mergeMaxAges(...$forbidden_cache_ages));
  }

}
