<?php

declare(strict_types = 1);

/**
 * Copyright (C) 2021 PRONOVIX GROUP.
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301,
 * USA.
 */

namespace Drupal\view_usernames\Type\Bypasser;

use Drupal\Core\Cache\CacheableDependencyInterface;

/**
 * Represents an immutable bypass or not decision.
 *
 * @internal This class is not part of the module's public programming API.
 */
abstract class BypasserResult {

  /**
   * The current state.
   *
   * @var bool
   */
  private bool $state;

  /**
   * Cacheability information for the decision is available/applicable.
   *
   * Nullable parameters should be avoided but having a cacheable and
   * non-cacheable decision did not seem to have an actual added value.
   *
   * @var \Drupal\Core\Cache\CacheableDependencyInterface|null
   *
   * @phpcs:disable SlevomatCodingStandard.Variables.UnusedVariable.UnusedVariable
   */
  private ?CacheableDependencyInterface $cacheability = NULL;

  /**
   * Constructs a new object.
   *
   * @param bool $state
   *   The state.
   */
  final protected function __construct(bool $state) {
    $this->state = $state;
  }

  /**
   * Current state as bool.
   *
   * @return bool
   *   TRUE or FALSE.
   */
  final public function asBool(): bool {
    return $this->state;
  }

  /**
   * Return cacheability information for the decision is available/applicable.
   *
   * @return \Drupal\Core\Cache\CacheableDependencyInterface|null
   *   Cacheability information for the decision is available/applicable
   */
  final public function getCacheability(): ?CacheableDependencyInterface {
    return $this->cacheability;
  }

  /**
   * Adds cacheability information to the decision.
   *
   * @param \Drupal\Core\Cache\CacheableDependencyInterface $cacheability
   *   Cacheability information.
   *
   * @return static
   *   New object with the expected cacheability.
   */
  final public function withCacheability(CacheableDependencyInterface $cacheability): BypasserResult {
    $that = clone $this;
    $that->cacheability = $cacheability;
    return $that;
  }

}
