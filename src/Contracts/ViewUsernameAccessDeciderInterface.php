<?php

declare(strict_types = 1);

/**
 * Copyright (C) 2021 - 2023 PRONOVIX GROUP.
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301,
 * USA.
 */

namespace Drupal\view_usernames\Contracts;

use Drupal\Core\Access\AccessResultAllowed;
use Drupal\Core\Access\AccessResultForbidden;
use Drupal\Core\Session\AccountInterface;
use Drupal\user\UserInterface;

/**
 * Contract for view username access deciders.
 */
interface ViewUsernameAccessDeciderInterface {

  /**
   * Checks if the acting user can view a username.
   *
   * @param \Drupal\Core\Session\AccountInterface $acting_user
   *   The acting user.
   * @param \Drupal\user\UserInterface $other_user
   *   The other user.
   *
   * @return \Drupal\Core\Access\AccessResultAllowed|\Drupal\Core\Access\AccessResultForbidden
   *   The access result. It has to be explicit forbidden or allowed but never
   *   neutral because it has a different meaning for fields and entities.
   *
   * @see \Drupal\view_usernames\DefaultViewUsernameAccessDecider::canViewUserName()
   */
  public function canViewUserName(AccountInterface $acting_user, UserInterface $other_user): AccessResultAllowed|AccessResultForbidden;

}
