<?php

declare(strict_types = 1);

/**
 * Copyright (C) 2022 PRONOVIX GROUP.
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301,
 * USA.
 */

namespace Drupal\view_usernames\Contracts;

use Drupal\Core\Cache\CacheableDependencyInterface;

/**
 * Contract for configurable view_usernames_user_format_name_alter() bypassers.
 */
interface ConfigurableUserFormatNameHardeningBypasserInterface extends UserFormatNameHardeningBypasserInterface {

  /**
   * Activate bypass.
   *
   * @param \Drupal\Core\Cache\CacheableDependencyInterface|null $cacheability
   *   Cacheability information that is applicable for the decision, if there
   *   is any.
   */
  public function activate(?CacheableDependencyInterface $cacheability = NULL): void;

  /**
   * Deactivates bypass.
   *
   * @param \Drupal\Core\Cache\CacheableDependencyInterface|null $cacheability
   *   Cacheability information that is applicable for the decision, if there
   *   is any.
   */
  public function deactivate(?CacheableDependencyInterface $cacheability = NULL): void;

}
