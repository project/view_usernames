<?php

declare(strict_types = 1);

/**
 * Copyright (C) 2022 PRONOVIX GROUP.
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301,
 * USA.
 */

namespace Drupal\view_usernames\Contracts;

use Drupal\view_usernames\Type\Bypasser\BypasserResult;

/**
 * Contract for view_usernames_user_format_name_alter() bypassers.
 */
interface UserFormatNameHardeningBypasserInterface {

  /**
   * Returns true if view_usernames_user_format_name_alter() should be bypassed.
   *
   * @return \Drupal\view_usernames\Type\Bypasser\BypasserResult
   *   The decision.
   */
  public function shouldByPass(): BypasserResult;

}
