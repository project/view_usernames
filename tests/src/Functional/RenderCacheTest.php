<?php

declare(strict_types = 1);

/**
 * Copyright (C) 2021-2023 PRONOVIX GROUP.
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301,
 * USA.
 */

namespace Drupal\Tests\view_usernames\Functional;

use Drupal\Tests\BrowserTestBase;
use Drupal\Tests\WebAssert;
use Drupal\Tests\view_usernames_render_cache_tester\Traits\RenderCacheTestTrait;
use Drupal\user\Entity\User;
use Drupal\view_usernames_test\SpecialUsernameViewAccessDecider;
use Drupal\view_usernames_test2\DummyUserFormatNameHardeningBypasser;

/**
 * Smoke tests for render caching.
 */
final class RenderCacheTest extends BrowserTestBase {

  use RenderCacheTestTrait;

  private const USERNAME_SIMPLE_USER = 'simple user';

  private const USERNAME_SPECIAL_USER = 'special user';

  private const USERNAME_USER_WITH_VIEW_USERNAMES_PERMISSION = 'user with view usernames permission';

  private const ROLE_ID_VIEW_USERNAMES_PERMISSION = 'role_with_view_usernames_permission';

  /**
   * {@inheritdoc}
   */
  protected $defaultTheme = 'stark';

  /**
   * {@inheritdoc}
   */
  protected static $modules = [
    'view_usernames_test',
    'view_usernames_render_cache_tester',
  ];

  /**
   * A simple registered user.
   *
   * @var \Drupal\user\UserInterface
   */
  private $simpleUser;

  /**
   * A user that can be viewed by anyone until it has a special role.
   *
   * @var \Drupal\user\UserInterface
   */
  private $userCanBeViewed;

  /**
   * A user with "view usernames" permission.
   *
   * @var \Drupal\user\UserInterface
   */
  private $userWithViewPermission;

  /**
   * {@inheritdoc}
   */
  protected function setUp(): void {
    parent::setUp();

    $this->simpleUser = $this->createUser([], self::USERNAME_SIMPLE_USER);

    $this->createRole(['view usernames'], self::ROLE_ID_VIEW_USERNAMES_PERMISSION);
    $this->userWithViewPermission = $this->createUser([], self::USERNAME_USER_WITH_VIEW_USERNAMES_PERMISSION);
    $this->userWithViewPermission->addRole(self::ROLE_ID_VIEW_USERNAMES_PERMISSION);
    $this->userWithViewPermission->save();

    $this->createRole([], SpecialUsernameViewAccessDecider::SPECIAL_USER_ROLE, SpecialUsernameViewAccessDecider::SPECIAL_USER_ROLE);
    $this->userCanBeViewed = $this->createUser([], self::USERNAME_SPECIAL_USER);
    $this->userCanBeViewed->addRole(SpecialUsernameViewAccessDecider::SPECIAL_USER_ROLE);
    $this->userCanBeViewed->save();
  }

  /**
   * Tests caching and cache invalidation on pages that list usernames.
   */
  public function testPageCacheabilityWithMultipleUsers(): void {
    $this->visitUsernameList();
    $this->assertUserNameVisibility(FALSE, TRUE, FALSE);

    // A logged-in user should see a different result.
    $this->drupalLogin($this->simpleUser);
    $this->visitUsernameList();
    $this->assertSession()->responseHeaderEquals('X-Drupal-Dynamic-Cache', 'MISS');
    $this->assertUserNameVisibility(TRUE, TRUE, FALSE);
    $this->visitUsernameList();
    $this->assertSession()->responseHeaderEquals('X-Drupal-Dynamic-Cache', 'HIT');
    // Without the special role, the username must not be visible anymore
    // for a user without "view usernames" permission. (Cache invalidation.)
    $this->userCanBeViewed->removeRole(SpecialUsernameViewAccessDecider::SPECIAL_USER_ROLE);
    $this->userCanBeViewed->save();
    $this->visitUsernameList();
    $this->assertUserNameVisibility(TRUE, FALSE, FALSE);

    // A different logged-in user should see a different result.
    $this->drupalLogin($this->userWithViewPermission);
    $this->visitUsernameList();
    $this->assertUserNameVisibility(TRUE, TRUE, TRUE);
  }

  /**
   * Tests cacheability on pages that outputs a username.
   */
  public function testPageCacheabilityWithOneUsername(): void {
    $anonymous_user = User::load(0);
    $this->assertOnUsernameViewPages(
      $anonymous_user,
      fn() => $this->assertUserNameVisibility(FALSE, FALSE, FALSE),
      fn() => $this->assertSession()->responseHeaderEquals('X-Drupal-Cache', 'MISS')
    );
    $this->assertOnUsernameViewPages(
      $anonymous_user,
      fn() => $this->assertSession()->responseHeaderEquals('X-Drupal-Cache', 'HIT'),
    );

    $this->drupalLogin($this->simpleUser);
    $this->assertOnUsernameViewPages(
      $this->simpleUser,
      fn() => $this->assertSession()->responseHeaderEquals('X-Drupal-Dynamic-Cache', 'MISS'),
    );
    $this->assertOnUsernameViewPages(
      $this->simpleUser,
      fn() => $this->assertSession()->responseHeaderEquals('X-Drupal-Dynamic-Cache', 'HIT'),
    );

    $this->drupalLogin($this->userWithViewPermission);
    $this->assertOnUsernameViewPages(
      $this->simpleUser,
      fn() =>  $this->assertSession()->responseHeaderEquals('X-Drupal-Dynamic-Cache', 'MISS'),
    );
    $this->assertOnUsernameViewPages(
      $this->simpleUser,
      fn() =>  $this->assertSession()->responseHeaderEquals('X-Drupal-Dynamic-Cache', 'HIT'),
    );
  }

  /**
   * Tests cacheability calculation and invalidation happens for the right user.
   */
  public function testCacheInvalidationHappensBasedOnActingUserNotCurrentUser(): void {
    $simple_user2_special_role_rid = 'simple_user2_special_role';
    $this->createRole(['access content'], $simple_user2_special_role_rid);
    $simple_user2 = $this->createUser([], 'simple user 2');
    $simple_user2->addRole($simple_user2_special_role_rid);
    $simple_user2->save();

    // Cacheability has to be calculated based on the acting user and not
    // the current user.
    $this->drupalLogin($simple_user2);
    // As the current user cannot be the same when "view as" feature is used
    // we have to verify the current user's access level on another page.
    $this->visitUsernameWithGetDisplayNamePage($this->simpleUser);
    $this->assertSession()->pageTextNotContains(self::USERNAME_SIMPLE_USER);

    $this->visitUsernameViewAsUser($this->simpleUser, $this->userWithViewPermission);
    $this->assertUserNameVisibility(TRUE, FALSE, FALSE, FALSE);
    $this->assertSession()->responseHeaderNotContains('X-Drupal-Cache-Tags', "user:{$simple_user2->id()}");
    $this->assertSession()->responseHeaderNotContains('X-Drupal-Cache-Tags', "config:user.role.{$simple_user2_special_role_rid}");

    user_role_revoke_permissions(self::ROLE_ID_VIEW_USERNAMES_PERMISSION, ['view usernames']);
    $this->visitUsernameViewAsUser($this->simpleUser, $this->userWithViewPermission);
    $this->assertUserNameVisibility(FALSE, FALSE, FALSE, FALSE);
    $this->visitUsernameWithGetDisplayNamePage($this->simpleUser);
    $this->assertSession()->pageTextNotContains(self::USERNAME_SIMPLE_USER);
    $this->assertSession()->responseHeaderEquals('X-Drupal-Dynamic-Cache', 'HIT');

    user_role_grant_permissions($simple_user2_special_role_rid, ['view usernames']);
    $this->visitUsernameViewAsUser($this->simpleUser, $this->userWithViewPermission);
    $this->assertUserNameVisibility(FALSE, FALSE, FALSE, FALSE);
    $this->visitUsernameWithGetDisplayNamePage($this->simpleUser);
    $this->assertSession()->pageTextContains(self::USERNAME_SIMPLE_USER);
    $this->assertSession()->responseHeaderEquals('X-Drupal-Dynamic-Cache', 'MISS');
  }

  /**
   * Tests cacheability on pages that outputs a username.
   *
   * @see \view_usernames_user_format_name_alter()
   */
  public function testCacheabilityBubbleUpsWhenUsernameIsExposedDirectly(): void {
    $assert_expected_cacheability = static function (WebAssert $session): void {
      $session->responseHeaderContains('X-Drupal-Cache-Contexts', SpecialUsernameViewAccessDecider::SPECIAL_CACHE_CONTEXT_FOR_SPECIAL_ROLE);
      $session->responseHeaderContains('X-Drupal-Cache-Contexts', SpecialUsernameViewAccessDecider::SPECIAL_CACHE_CONTEXT_FOR_NOT_PROTECTED_USERNAME);
      $session->responseHeaderContains('X-Drupal-Cache-Tags', DummyUserFormatNameHardeningBypasser::CONFIG_NAME);
    };

    $this->container->get('module_installer')->install(['view_usernames_test2']);
    // This is necessary, otherwise config schema checker fails...
    $this->container->get('config.typed')->clearCachedDefinitions();

    $this->drupalLogin($this->simpleUser);
    $this->visitUsernameWithGetDisplayNamePage($this->userCanBeViewed);
    $assert_expected_cacheability($this->assertSession());
    $this->assertSession()->pageTextContains(self::USERNAME_SPECIAL_USER);

    // Without the special role, the username must not be visible anymore
    // for a user without "view usernames" permission. (Cache invalidation.)
    $this->userCanBeViewed->removeRole(SpecialUsernameViewAccessDecider::SPECIAL_USER_ROLE);
    $this->userCanBeViewed->save();
    $this->visitUsernameWithGetDisplayNamePage($this->userCanBeViewed);
    $this->assertSession()->pageTextNotContains(self::USERNAME_SPECIAL_USER);
    $assert_expected_cacheability($this->assertSession());

    // When hardening is bypassed by force, usernames also become visible...
    $this->config(DummyUserFormatNameHardeningBypasser::CONFIG_NAME)->set('active', TRUE)->save();
    $this->visitUsernameWithGetDisplayNamePage($this->userCanBeViewed);
    $this->assertSession()->pageTextContains(self::USERNAME_SPECIAL_USER);
    $this->assertSession()->responseHeaderContains('X-Drupal-Cache-Tags', DummyUserFormatNameHardeningBypasser::CONFIG_NAME);

    // And when it is disabled, restrictions are back.
    $this->config(DummyUserFormatNameHardeningBypasser::CONFIG_NAME)->set('active', FALSE)->save();
    $this->visitUsernameWithGetDisplayNamePage($this->userCanBeViewed);
    $this->assertSession()->pageTextNotContains(self::USERNAME_SPECIAL_USER);
    $this->assertSession()->responseHeaderContains('X-Drupal-Cache-Tags', DummyUserFormatNameHardeningBypasser::CONFIG_NAME);
  }

  /**
   * Asserts the expected usernames are visible on the page.
   *
   * @param bool $is_simple_user_visible
   *   Whether the simple user is visible or not.
   * @param bool $is_special_user_visible
   *   Whether the special user is visible or not.
   * @param bool $is_admin_visible
   *   Whether the admin (uid1) is visible or not.
   * @param bool $is_anonymous_visible
   *   Whether the anonymous (uid0) is visible or not. Usually it is visible
   *   therefore the default value is TRUE.
   */
  private function assertUserNameVisibility(bool $is_simple_user_visible, bool $is_special_user_visible, bool $is_admin_visible, bool $is_anonymous_visible = TRUE): void {
    $is_anonymous_visible
      ? $this->assertSession()->pageTextContains('Anonymous')
      : $this->assertSession()->pageTextNotContains('Anonymous');
    $is_simple_user_visible
        ? $this->assertSession()->pageTextContains(self::USERNAME_SIMPLE_USER)
        : $this->assertSession()->pageTextNotContains(self::USERNAME_SIMPLE_USER);
    $is_special_user_visible
        ? $this->assertSession()->pageTextContains(self::USERNAME_SPECIAL_USER)
        : $this->assertSession()->pageTextNotContains(self::USERNAME_SPECIAL_USER);
    $is_admin_visible
        ? $this->assertSession()->pageTextContains('admin')
        : $this->assertSession()->pageTextNotContains('admin');
  }

}
