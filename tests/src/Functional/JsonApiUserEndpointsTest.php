<?php

declare(strict_types = 1);

/**
 * Copyright (C) 2022 PRONOVIX GROUP.
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301,
 * USA.
 */

namespace Drupal\Tests\view_usernames\Functional;

use Drupal\Core\Url;
use Drupal\Tests\BrowserTestBase;
use Drupal\user\UserInterface;

/**
 * Ensures User related JSON API endpoints are still working as expected.
 *
 * View Usernames used to cause an early rendering issue that caused HTTP 500
 * on user related endpoints. This class contains plain simple test cases
 * that ensure this issue is fixed. Since JSON API MUST use the same access
 * control rules as other parts of the system therefore we assume the other
 * tests cover the rest. (e.g.: whether it exposes a username to a given user
 * or not).
 *
 * @covers \Drupal\view_usernames\EventSubscriber\JsonApiEarlyRenderingFixEventSubscriber
 *
 * @see https://project.pronovix.net/issues/15284
 */
final class JsonApiUserEndpointsTest extends BrowserTestBase {

  /**
   * {@inheritdoc}
   */
  protected $defaultTheme = 'stark';

  private const USERNAME_SIMPLE_USER = 'simple user';

  /**
   * {@inheritdoc}
   */
  protected static $modules = [
    'view_usernames_test',
    'jsonapi',
  ];

  /**
   * A simple registered user.
   *
   * @var \Drupal\user\UserInterface
   */
  private UserInterface $simpleUser;

  /**
   * {@inheritdoc}
   */
  protected function setUp(): void {
    parent::setUp();

    $this->simpleUser = $this->createUser([], self::USERNAME_SIMPLE_USER);
  }

  /**
   * Tests the user collection endpoints works for visitors and logged-in users.
   *
   * (One test to reduce the amount of necessary site installs.)
   */
  public function testUserListingEndpointWorksForVisitorsAndLoggedInUsers(): void {
    self::assertTrue($this->container->get('current_user')->isAnonymous());
    $this->visitJsonapiUserCollectionEndpoint();
    $this->assertSession()->statusCodeEquals(200);

    $this->drupalLogin($this->simpleUser);
    self::assertTrue($this->container->get('current_user')->isAuthenticated());
    $this->visitJsonapiUserCollectionEndpoint();
    $this->assertSession()->statusCodeEquals(200);
  }

  /**
   * Tests user can access own user profile data via JSON API.
   */
  public function testUserCanAccessOwnUserProfileData(): void {
    $this->drupalLogin($this->simpleUser);
    self::assertTrue($this->container->get('current_user')->isAuthenticated());
    $this->visitJsonapiUserCanonicalEndpoint($this->simpleUser->uuid());
    $this->assertSession()->statusCodeEquals(200);
  }

  /**
   * Visits the JSON API user collection endpoint (/jsonapi/user/user).
   */
  private function visitJsonapiUserCollectionEndpoint(): void {
    $url = Url::fromRoute('jsonapi.user--user.collection');
    $this->drupalGet($url->toString());
  }

  /**
   * Visits the JSON API user canonical endpoint (/jsonapi/user/user/{user}).
   *
   * @param string $uuid
   *   The UUID of a user.
   */
  private function visitJsonapiUserCanonicalEndpoint(string $uuid): void {
    $url = Url::fromRoute('jsonapi.user--user.individual', ['entity' => $uuid]);
    $this->drupalGet($url->toString());
  }

}
