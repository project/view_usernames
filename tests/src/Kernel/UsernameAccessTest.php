<?php

declare(strict_types = 1);

/**
 * Copyright (C) 2021 PRONOVIX GROUP.
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301,
 * USA.
 */

namespace Drupal\Tests\view_usernames\Kernel;

use Drupal\Tests\user\Traits\UserCreationTrait;
use Drupal\user\RoleInterface;
use Drupal\view_usernames_test\SpecialUsernameViewAccessDecider;

/**
 * Ensures that entity and field access works as expected with this module.
 *
 * This test also assert on the original behavior by design. We changed a core
 * functionally of Drupal core, and we would like to be sure that our tests
 * only pass because our code is working. In addition, we should know if
 * the original behavior changed in Drupal because it can make our module
 * obsolete, or it may require further adjustments on our code.
 *
 * @covers \Drupal\view_usernames\DefaultViewUsernameAccessDecider
 */
final class UsernameAccessTest extends UsernameAccessTestKernelTestBase {

  use UserCreationTrait {
    createUser as drupalCreateUser;
  }

  /**
   * Uid1.
   *
   * @var \Drupal\user\UserInterface
   */
  private $uid1;

  /**
   * A simple registered user.
   *
   * @var \Drupal\user\UserInterface
   */
  private $simpleUser;

  /**
   * {@inheritdoc}
   */
  protected function setUp(): void {
    parent::setUp();

    // Clear permissions for authenticated users.
    $this->config('user.role.' . RoleInterface::AUTHENTICATED_ID)
      ->set('permissions', [])
      ->save();

    // The id needs to be declared explicitly here, because with PostgreSQL id
    // generation works differently and the first generated id is 2.
    $this->uid1 = $this->drupalCreateUser([], 'UID1', FALSE, ['uid' => 1]);

    $this->simpleUser = $this->drupalCreateUser([], 'simple user');
  }

  /**
   * The special uid1 user can still access everything. #Drupalism.
   */
  public function testUid1StillHaveBypassAccess(): void {
    $this->assertUserCouldAccessOtherUsersUsername($this->simpleUser, $this->uid1);
    $this->enableViewUsernamesModule();
    $this->assertUserCanAccessOtherUsersUsername($this->simpleUser, $this->uid1);
  }

  /**
   * Anonymous user cannot access registered users' username by default.
   */
  public function testAnonymousUserCannotAccessLoggedInUsersUsernames(): void {
    self::assertEquals(0, $this->container->get('current_user')->id());
    $this->assertUserCouldAccessOtherUsersUsername($this->simpleUser);
    $this->enableViewUsernamesModule();
    $this->assertUserCanNotAccessOtherUsersUsername($this->simpleUser);
  }

  /**
   * A registered user cannot access other reg. user's username by default.
   */
  public function testRegisteredUserCannotAccessOtherRegisteredUsersUsernames(): void {
    $other_logged_in_user = $this->createUser([], 'other simple user');
    $this->assertUserCouldAccessOtherUsersUsername($this->simpleUser, $other_logged_in_user);
    $this->enableViewUsernamesModule();
    $this->assertUserCanNotAccessOtherUsersUsername($this->simpleUser, $other_logged_in_user);
  }

  /**
   * A user with "view usernames" permission can see other users' username.
   */
  public function testUserWithViewUsernamesPermissionCanAccessOtherRegisteredUsersUsernames(): void {
    $this->enableViewUsernamesModule();
    $other_logged_in_user = $this->createUser(['view usernames'], 'user with view usernames permission');
    $this->assertUserCanAccessOtherUsersUsername($this->simpleUser, $other_logged_in_user);
  }

  /**
   * A user with "administer users" permission can see other users' username.
   */
  public function testUserWithAdministerUsersPermissionCanAccessOtherRegisteredUsersUsernames(): void {
    $this->enableViewUsernamesModule();
    $sub_admin = $this->createUser(['administer users'], 'user with administer users permission');
    $this->assertUserCanAccessOtherUsersUsername($this->simpleUser, $sub_admin);
  }

  /**
   * Users can always view their own usernames.
   */
  public function testUserCanAlwaysAccessOwnUsername(): void {
    $this->enableViewUsernamesModule();
    $this->assertUserCanAccessOtherUsersUsername($this->simpleUser, $this->simpleUser);
  }

  /**
   * Anonymous user's username is visible to everyone.
   */
  public function testEverybodyCanAccessAnonymousUsersUsername(): void {
    $this->enableViewUsernamesModule();
    $uid0 = $this->container->get('entity_type.manager')->getStorage('user')->load(0);
    self::assertEquals(0, $this->container->get('current_user')->id());
    $this->assertUserCanAccessOtherUsersUsername($uid0);
    // If authenticated role have access then we can assume that everyone does.
    $this->assertUserCanAccessOtherUsersUsername($uid0, $this->simpleUser);
  }

  /**
   * Ensure that other modules can grant access to usernames.
   */
  public function testOtherModulesCanGrantAccess(): void {
    $this->enableViewUsernamesModule();
    $role_id = $this->createRole([], SpecialUsernameViewAccessDecider::SPECIAL_USER_ROLE, SpecialUsernameViewAccessDecider::SPECIAL_USER_ROLE);
    $user = $this->drupalCreateUser([], SpecialUsernameViewAccessDecider::USERNAME_WITH_NO_PROTECTION);
    $user->addRole($role_id);
    $user->save();
    // Reload the user object with the new role.
    $this->assertUserCouldNotAccessOtherUsersUsername($user, $this->simpleUser);
    $this->enableModules(['view_usernames_test']);
    // We need the updated access control handler.
    $this->accessHandler = $this->container->get('entity_type.manager')->getAccessControlHandler('user');
    $this->assertUserCanAccessOtherUsersUsername($user, $this->simpleUser);

    // Assert that both access results were applied on the final result.
    // @see \Drupal\view_usernames_test\EventSubscriber\ExposeSpecialUsernamesEventSubscriber::grantAccess()
    /** @var \Drupal\Core\Access\AccessResult $result */
    $result = $this->accessHandler->access($user, 'view label', $this->simpleUser, TRUE);
    self::assertContains(SpecialUsernameViewAccessDecider::SPECIAL_CACHE_CONTEXT_FOR_SPECIAL_ROLE, $result->getCacheContexts(), 'Expected cache context is added for special role.');
    self::assertContains(SpecialUsernameViewAccessDecider::SPECIAL_CACHE_CONTEXT_FOR_NOT_PROTECTED_USERNAME, $result->getCacheContexts(), 'Expected cache context is added for special user.');
  }

}
