<?php

declare(strict_types = 1);

/**
 * Copyright (C) 2021 PRONOVIX GROUP.
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301,
 * USA.
 */

namespace Drupal\Tests\view_usernames\Kernel;

use Drupal\Core\Session\AccountInterface;

/**
 * Run some sanity checks to confirm our username render fix is worked.
 *
 * Testing all possible access scenarios is out of scope of this test.
 */
final class UsernameRenderFixTest extends UsernameLeakMitigationFixTestBase {

  /**
   * {@inheritdoc}
   */
  protected function assertUsernameHasNotChanged(AccountInterface $user): void {
    $build = [
      '#theme' => 'username',
      '#account' => $user,
    ];
    $result = $this->container->get('renderer')->renderRoot($build);
    self::assertNotEmpty(strip_tags((string) $result), "The username of {$user->getAccountName()} has not been changed for {$this->container->get('current_user')->getAccountName()}.");
  }

  /**
   * {@inheritdoc}
   */
  protected function assertUsernameWasRemoved(AccountInterface $user): void {
    $build = [
      '#theme' => 'username',
      '#account' => $user,
    ];
    $result = $this->container->get('renderer')->renderRoot($build);
    self::assertEquals('', strip_tags((string) $result), "The username of {$user->getAccountName()} has been removed for {$this->container->get('current_user')->getAccountName()}.");
  }

}
