<?php

declare(strict_types = 1);

/**
 * Copyright (C) 2022 PRONOVIX GROUP.
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301,
 * USA.
 */

namespace Drupal\Tests\view_usernames\Kernel;

use Drupal\Core\Test\AssertMailTrait;
use Drupal\Tests\user\Traits\UserCreationTrait;

/**
 * Ensures email sending still works and username replacement is available.
 *
 * @covers \Drupal\view_usernames\TemporaryUserFormatNameHardeningBypasser
 * @covers \Drupal\view_usernames\Mail\MailManagerDecorator
 * @covers \Drupal\view_usernames\DependencyInjection\MailPluginDecoratorPass
 * @covers \Drupal\view_usernames\ViewUsernamesServiceProvider
 */
final class EmailSendingTest extends UsernameAccessTestKernelTestBase {

  use AssertMailTrait;
  use UserCreationTrait {
    createUser as drupalCreateUser;
  }

  private const SIMPLE_USER_USERNAME = 'simple user';

  /**
   * A simple registered user.
   *
   * @var \Drupal\user\UserInterface
   */
  private $simpleUser;

  /**
   * {@inheritdoc}
   */
  protected static $modules = [
    'view_usernames',
    'view_usernames_test',
  ];

  /**
   * {@inheritdoc}
   */
  protected function setUp(): void {
    parent::setUp();

    // Create user 1 who has special permissions.
    $this->drupalCreateUser([], 'UID1');

    $this->simpleUser = $this->drupalCreateUser([], self::SIMPLE_USER_USERNAME);
  }

  /**
   * Tests email sending works and username is visible in the outgoing email.
   */
  public function testUsernameIsVisibleInMail(): void {
    $this->sendTestEmail();
    $this->assertEmailContentIsCorrect();
  }

  /**
   * Tests that the temporary bypass while an email is sent is temporary.
   */
  public function testTemporaryBypassIsOnlyActiveWhileEmailIsSent(): void {
    $uid0 = $this->container->get('entity_type.manager')->getStorage('user')->load(0);
    self::assertEquals(0, $this->container->get('current_user')->id());
    $this->assertUserCouldNotAccessOtherUsersUsername($this->simpleUser, $uid0);

    $this->sendTestEmail();

    $this->assertUserCannotAccessOtherUsersUsername($this->simpleUser, $uid0);
  }

  /**
   * Sends a test email to simple user.
   */
  private function sendTestEmail(): void {
    $mailer = $this->container->get('plugin.manager.mail');
    $mailer->mail('view_usernames_test', '', $this->simpleUser->getEmail(), 'en', ['user' => $this->simpleUser]);
  }

  /**
   * Asserts that simple user got an email with correct content.
   */
  private function assertEmailContentIsCorrect(): void {
    $this->assertMailString('body', sprintf('Dear %s, username token replacement is still working.', self::SIMPLE_USER_USERNAME), 1, 'Username is visible in the email body.');
  }

}
