<?php

declare(strict_types = 1);

/**
 * Copyright (C) 2023 PRONOVIX GROUP.
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301,
 * USA.
 */

namespace Drupal\Tests\view_usernames\Kernel;

use Drupal\Core\Access\AccessResultAllowed;
use Drupal\Core\Access\AccessResultForbidden;
use Drupal\Core\DependencyInjection\ClassResolverInterface;
use Drupal\Core\Session\AccountInterface;
use Drupal\user\UserInterface;
use Drupal\view_usernames\Contracts\ViewUsernameAccessDeciderInterface;
use Drupal\view_usernames\ViewUsernameAccessDeciderCollector;

/**
 * Tests the decider collector.
 *
 * @covers \Drupal\view_usernames\ViewUsernameAccessDeciderCollector
 */
final class ViewUsernameAccessDeciderCollectorTest extends UsernameAccessTestKernelTestBase {

  /**
   * Tests that at least one decider have to be configured.
   */
  public function testAtLeastOneDeciderHaveToBeConfigured(): void {
    $this->expectExceptionMessage('The list of decider service IDs MUST NOT be empty.');
    // expectErrorMessage() is deprecated.
    // @see https://github.com/sebastianbergmann/phpunit/issues/5062
    set_error_handler(static function (int $err_no, string $error_str): void {
      throw new \Exception($error_str, $err_no);
    }, E_USER_WARNING);

    try {
      new ViewUsernameAccessDeciderCollector($this->container->get('class_resolver'), []);
    }
    finally {
      restore_error_handler();
    }
  }

  /**
   * Tests that deciders are called in order.
   */
  public function testDecidersAreCalledInOrder(): void {
    $a_access_result = AccessResultAllowed::allowed()->addCacheTags(['a']);
    $b_access_result = AccessResultForbidden::forbidden()->addCacheTags(['b']);
    $c_access_result = AccessResultAllowed::allowed()->addCacheTags(['c']);
    self::assertNotEquals($a_access_result, $b_access_result);
    self::assertNotEquals($b_access_result, $c_access_result);
    self::assertNotEquals($a_access_result, $c_access_result);

    $resolver = new class ($a_access_result, $b_access_result, $c_access_result) implements ClassResolverInterface {

      /**
       * @var \Drupal\Core\Access\AccessResultAllowed|\Drupal\Core\Access\AccessResultForbidden
       */
      private AccessResultAllowed|AccessResultForbidden $a;

      /**
       * @var \Drupal\Core\Access\AccessResultAllowed|\Drupal\Core\Access\AccessResultForbidden
       */
      private AccessResultAllowed|AccessResultForbidden $b;

      /**
       * @var \Drupal\Core\Access\AccessResultAllowed|\Drupal\Core\Access\AccessResultForbidden
       */
      private AccessResultAllowed|AccessResultForbidden $c;

      /**
       * Constructs a new object.
       *
       * @param \Drupal\Core\Access\AccessResultAllowed|\Drupal\Core\Access\AccessResultForbidden $a
       *   One access result.
       * @param \Drupal\Core\Access\AccessResultAllowed|\Drupal\Core\Access\AccessResultForbidden $b
       *   The another.
       * @param \Drupal\Core\Access\AccessResultAllowed|\Drupal\Core\Access\AccessResultForbidden $c
       *   And another.
       */
      public function __construct(AccessResultAllowed|AccessResultForbidden $a, AccessResultAllowed|AccessResultForbidden $b, AccessResultAllowed|AccessResultForbidden $c) {
        $this->a = $a;
        $this->b = $b;
        $this->c = $c;
      }

      /**
       * {@inheritdoc}
       */
      public function getInstanceFromDefinition($definition): object {
        $a_access_result = $this->a;
        $b_access_result = $this->b;
        $c_access_result = $this->c;

        $a_decider = new class ($a_access_result) implements ViewUsernameAccessDeciderInterface {

          /**
           * The access result to return.
           *
           * @var \Drupal\Core\Access\AccessResultAllowed|\Drupal\Core\Access\AccessResultForbidden
           */
          private AccessResultAllowed|AccessResultForbidden $result;

          /**
           * Constructs a new object.
           *
           * @param \Drupal\Core\Access\AccessResultAllowed|\Drupal\Core\Access\AccessResultForbidden $result
           *   The access result to return.
           */
          public function __construct(AccessResultAllowed|AccessResultForbidden $result) {
            $this->result = $result;
          }

          /**
           * {@inheritdoc}
           */
          public function canViewUserName(AccountInterface $acting_user, UserInterface $other_user): AccessResultAllowed|AccessResultForbidden {
            return $this->result;
          }

        };
        $b_decider = new class ($b_access_result) implements ViewUsernameAccessDeciderInterface {

          /**
           * The access result to return.
           *
           * @var \Drupal\Core\Access\AccessResultAllowed|\Drupal\Core\Access\AccessResultForbidden
           */
          private AccessResultAllowed|AccessResultForbidden $result;

          /**
           * Constructs a new object.
           *
           * @param \Drupal\Core\Access\AccessResultAllowed|\Drupal\Core\Access\AccessResultForbidden $result
           *   The access result to return.
           */
          public function __construct(AccessResultAllowed|AccessResultForbidden $result) {
            $this->result = $result;
          }

          /**
           * {@inheritdoc}
           */
          public function canViewUserName(AccountInterface $acting_user, UserInterface $other_user): AccessResultAllowed|AccessResultForbidden {
            return $this->result;
          }

        };
        $c_decider = new class ($c_access_result) implements ViewUsernameAccessDeciderInterface {

          /**
           * The access result to return.
           *
           * @var \Drupal\Core\Access\AccessResultAllowed|\Drupal\Core\Access\AccessResultForbidden
           */
          private AccessResultAllowed|AccessResultForbidden $result;

          /**
           * Constructs a new object.
           *
           * @param \Drupal\Core\Access\AccessResultAllowed|\Drupal\Core\Access\AccessResultForbidden $result
           *   The access result to return.
           */
          public function __construct(AccessResultAllowed|AccessResultForbidden $result) {
            $this->result = $result;
          }

          /**
           * {@inheritdoc}
           */
          public function canViewUserName(AccountInterface $acting_user, UserInterface $other_user): AccessResultAllowed|AccessResultForbidden {
            return $this->result;
          }

        };

        return match ($definition) {
          'a_decider' => $a_decider,
          'b_decider' => $b_decider,
          'c_decider' => $c_decider,
          default => throw new \LogicException("Unhandled definition: $definition"),
        };
      }

    };

    $anonymous = $this->container->get('entity_type.manager')->getStorage('user')->load(0);

    $collector = new ViewUsernameAccessDeciderCollector($resolver, [
      'a_decider',
      'b_decider',
      'c_decider',
    ]);
    self::assertEquals($a_access_result, $collector->canViewUserName($anonymous, $anonymous));

    $collector = new ViewUsernameAccessDeciderCollector($resolver, [
      'b_decider',
      'a_decider',
      'c_decider',
    ]);
    self::assertEquals($a_access_result, $collector->canViewUserName($anonymous, $anonymous));

    $collector = new ViewUsernameAccessDeciderCollector($resolver, [
      'c_decider',
      'b_decider',
      'a_decider',
    ]);
    self::assertEquals($c_access_result, $collector->canViewUserName($anonymous, $anonymous));
  }

  /**
   * Tests that cacheability information on forbidden results are aggregated.
   */
  public function testCacheabilityOnForbiddenResultsAreAggregated(): void {
    $a_access_result = AccessResultForbidden::forbidden()
      ->addCacheContexts(['user'])
      ->addCacheTags(['a-tag'])
      ->setCacheMaxAge(1);
    $b_access_result = AccessResultForbidden::forbidden()
      ->addCacheContexts(['timezone'])
      ->addCacheTags(['b-tag'])
      ->setCacheMaxAge(2);
    self::assertNotEquals($a_access_result->getCacheContexts(), $b_access_result->getCacheContexts());
    self::assertNotEquals($a_access_result->getCacheTags(), $b_access_result->getCacheTags());
    self::assertNotEquals($a_access_result->getCacheMaxAge(), $b_access_result->getCacheMaxAge());

    $resolver = new class ($a_access_result, $b_access_result) implements ClassResolverInterface {

      /**
       * @var \Drupal\Core\Access\AccessResultForbidden
       */
      private AccessResultForbidden $a;

      /**
       * @var \Drupal\Core\Access\AccessResultForbidden
       */
      private AccessResultForbidden $b;

      /**
       * Constructs a new object.
       *
       * @param \Drupal\Core\Access\AccessResultForbidden $a
       *   One access result.
       * @param \Drupal\Core\Access\AccessResultForbidden $b
       *   The another.
       */
      public function __construct(AccessResultForbidden $a, AccessResultForbidden $b) {
        $this->a = $a;
        $this->b = $b;
      }

      /**
       * {@inheritdoc}
       */
      public function getInstanceFromDefinition($definition): object {
        $a_access_result = $this->a;
        $b_access_result = $this->b;

        $a_decider = new class ($a_access_result) implements ViewUsernameAccessDeciderInterface {

          /**
           * The access result to return.
           *
           * @var \Drupal\Core\Access\AccessResultForbidden
           */
          private AccessResultForbidden $result;

          /**
           * Constructs a new object.
           *
           * @param \Drupal\Core\Access\AccessResultForbidden $result
           *   The access result to return.
           */
          public function __construct(AccessResultForbidden $result) {
            $this->result = $result;
          }

          /**
           * {@inheritdoc}
           */
          public function canViewUserName(AccountInterface $acting_user, UserInterface $other_user): AccessResultAllowed|AccessResultForbidden {
            return $this->result;
          }

        };
        $b_decider = new class ($b_access_result) implements ViewUsernameAccessDeciderInterface {

          /**
           * The access result to return.
           *
           * @var \Drupal\Core\Access\AccessResultForbidden
           */
          private AccessResultForbidden $result;

          /**
           * Constructs a new object.
           *
           * @param \Drupal\Core\Access\AccessResultForbidden $result
           *   The access result to return.
           */
          public function __construct(AccessResultForbidden $result) {
            $this->result = $result;
          }

          /**
           * {@inheritdoc}
           */
          public function canViewUserName(AccountInterface $acting_user, UserInterface $other_user): AccessResultAllowed|AccessResultForbidden {
            return $this->result;
          }

        };

        return match ($definition) {
          'a_decider' => $a_decider,
          'b_decider' => $b_decider,
          default => throw new \LogicException("Unhandled definition: $definition"),
        };
      }

    };

    $anonymous = $this->container->get('entity_type.manager')->getStorage('user')->load(0);

    $collector = new ViewUsernameAccessDeciderCollector($resolver, [
      'a_decider',
      'b_decider',
    ]);
    self::assertTrue($collector->canViewUserName($anonymous, $anonymous)->isForbidden());
    self::assertEquals(['user', 'timezone'], $collector->canViewUserName($anonymous, $anonymous)->getCacheContexts());
    self::assertEquals(['a-tag', 'b-tag'], $collector->canViewUserName($anonymous, $anonymous)->getCacheTags());
    self::assertEquals(1, $collector->canViewUserName($anonymous, $anonymous)->getCacheMaxAge());
  }

}
