<?php

declare(strict_types = 1);

/**
 * Copyright (C) 2022-2023 PRONOVIX GROUP.
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301,
 * USA.
 */

namespace Drupal\Tests\view_usernames\Kernel;

use Drupal\Core\Session\AccountInterface;
use Drupal\KernelTests\KernelTestBase;
use Drupal\user\UserInterface;

/**
 * Base class for username access tests.
 */
abstract class UsernameAccessTestKernelTestBase extends KernelTestBase {

  /**
   * {@inheritdoc}
   */
  protected static $modules = [
    'system',
    'user',
  ];

  /**
   * Access handler.
   *
   * @var \Drupal\Core\Entity\EntityAccessControlHandlerInterface
   */
  protected $accessHandler;

  /**
   * {@inheritdoc}
   */
  protected function setUp(): void {
    parent::setUp();

    $this->installSchema('system', 'sequences');
    $this->installEntitySchema('user');
    // We need the default label of the anonymous user.
    $this->installConfig('user');
    $entity_type_manager = $this->container->get('entity_type.manager');
    $this->accessHandler = $entity_type_manager->getAccessControlHandler('user');

    // Do the same trick as user_install() does.
    // We need this database entry to load uid0 with user storage.
    $entity_type_manager->getStorage('user')->create([
      'uid' => 0,
      'status' => 0,
      'name' => '',
    ])->save();

    // Ensure the username of the user entity that belongs to the Anonymous
    // user is initialized. This is a quick life hack for cleaner error
    // messages.
    /** @var \Drupal\user\UserInterface $anonymous */
    $anonymous = $this->container->get('entity_type.manager')->getStorage('user')->load(0);
    $anonymous->setUsername($this->config('user.settings')->get('anonymous'))->save();
  }

  /**
   * Enables the view_usernames module.
   */
  final protected function enableViewUsernamesModule(): void {
    $this->enableModules(['view_usernames']);
    // We need the updated user access handler with our access alters
    // registered.
    $this->accessHandler = $this->container->get('entity_type.manager')->getAccessControlHandler('user');
  }

  /**
   * Asserts a user (currently) can access to other user's username.
   *
   * @param \Drupal\user\UserInterface $other_user
   *   The other user.
   * @param \Drupal\Core\Session\AccountInterface|null $acting_user
   *   The acting user, by default, the current user.
   */
  final protected function assertUserCanAccessOtherUsersUsername(UserInterface $other_user, ?AccountInterface $acting_user = NULL): void {
    $this->assertUsernameAccess(TRUE, $other_user, $acting_user);
  }

  /**
   * Asserts a user could access to other user's username before.
   *
   * @param \Drupal\user\UserInterface $other_user
   *   The other user.
   * @param \Drupal\Core\Session\AccountInterface|null $acting_user
   *   The acting user, by default, the current user.
   */
  final protected function assertUserCouldAccessOtherUsersUsername(UserInterface $other_user, ?AccountInterface $acting_user = NULL): void {
    $this->assertUsernameAccess(TRUE, $other_user, $acting_user, TRUE);
  }

  /**
   * Asserts a user (currently) cannot access to other user's username.
   *
   * @param \Drupal\user\UserInterface $other_user
   *   The other user.
   * @param \Drupal\Core\Session\AccountInterface|null $acting_user
   *   The acting user, by default, the current user.
   */
  final protected function assertUserCanNotAccessOtherUsersUsername(UserInterface $other_user, ?AccountInterface $acting_user = NULL): void {
    $this->assertUsernameAccess(FALSE, $other_user, $acting_user);
  }

  /**
   * Asserts a user could not access to other user's username before.
   *
   * @param \Drupal\user\UserInterface $other_user
   *   The other user.
   * @param \Drupal\Core\Session\AccountInterface|null $acting_user
   *   The acting user, by default, the current user.
   */
  final protected function assertUserCouldNotAccessOtherUsersUsername(UserInterface $other_user, ?AccountInterface $acting_user = NULL): void {
    $this->assertUsernameAccess(FALSE, $other_user, $acting_user, TRUE);
  }

  /**
   * Assert on expected access to the username.
   *
   * @param bool $is_access_expected
   *   Whether access is granted or not.
   * @param \Drupal\user\UserInterface $other_user
   *   The other user.
   * @param \Drupal\Core\Session\AccountInterface|null $acting_user
   *   The acting user, by default, the current user.
   * @param bool $is_past_tense
   *   If the message should be in a past tense or not.
   */
  final protected function assertUsernameAccess(bool $is_access_expected, UserInterface $other_user, ?AccountInterface $acting_user = NULL, bool $is_past_tense = FALSE): void {
    // We are intentionally not overriding $acting_user if it is null, we let
    // the underlying code (access manager) to do what it used to.
    $current_user = $acting_user ?: $this->container->get('current_user');
    // We intentionally use getAccountName() here because that does not trigger
    // access check via view_usernames_user_format_name_alter().
    $acting_user_username = ucfirst($current_user->getAccountName());
    $other_user_reference = $current_user->id() == $other_user->id()
      ? 'own'
      : $other_user->getAccountName() . " user's";
    $can_or_could = $is_past_tense ? 'could' : 'can';
    $can_or_could .= $is_access_expected ? '' : ' not';
    $still_or_before = $is_past_tense ? 'before' : 'still';
    self::assertEquals($is_access_expected, $this->accessHandler->access($other_user, 'view label', $acting_user, TRUE)->isAllowed(), "{$acting_user_username} {$can_or_could} access {$other_user_reference} username {$still_or_before} as entity label.");
    self::assertEquals($is_access_expected, $this->accessHandler->fieldAccess('view', $other_user->name->getFieldDefinition(), $acting_user, $other_user->name, TRUE)->isAllowed(), "{$acting_user_username} {$can_or_could} access {$other_user_reference} username {$still_or_before} as a field.");

    $this->postAssertUsernameAccess();
  }

  /**
   * Allow subclasses to react after username access was tested.
   */
  protected function postAssertUsernameAccess(): void {}

}
