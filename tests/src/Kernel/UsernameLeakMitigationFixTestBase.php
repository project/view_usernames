<?php

declare(strict_types = 1);

/**
 * Copyright (C) 2021 PRONOVIX GROUP.
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301,
 * USA.
 */

namespace Drupal\Tests\view_usernames\Kernel;

use Drupal\Core\Session\AccountInterface;
use Drupal\KernelTests\KernelTestBase;
use Drupal\Tests\user\Traits\UserCreationTrait;

/**
 * Base class for testing mitigations for leaking usernames.
 */
abstract class UsernameLeakMitigationFixTestBase extends KernelTestBase {
  use UserCreationTrait {
    createUser as drupalCreateUser;
  }

  /**
   * {@inheritdoc}
   */
  protected static $modules = [
    'system',
    'user',
    'view_usernames',
  ];

  /**
   * A simple registered user.
   *
   * @var \Drupal\user\UserInterface
   */
  private $simpleUser;

  /**
   * A user with view username permission.
   *
   * @var \Drupal\user\UserInterface
   */
  private $userWithViewPermission;

  /**
   * {@inheritdoc}
   */
  protected function setUp(): void {
    parent::setUp();

    $this->installSchema('system', 'sequences');
    $this->installEntitySchema('user');
    // We need the default label of the anonymous user.
    $this->installConfig('user');

    // Reserve the user 1 who has special permissions by default.
    $this->drupalCreateUser([], 'UID1');
    $this->simpleUser = $this->drupalCreateUser([], 'simple user');
    $this->userWithViewPermission = $this->drupalCreateUser(['view usernames'], 'user with view usernames permission');
  }

  /**
   * Smoke testing mitigations in place.
   */
  final public function testMitigation(): void {
    self::assertEquals(0, $this->container->get('current_user')->id());

    // Anonymous user's username can be always disclosed.
    $this->assertUsernameHasNotChanged(\Drupal::currentUser());
    // Anonymous cannot access logged-in users' username.
    $this->assertUsernameWasRemoved($this->simpleUser);

    $this->setCurrentUser($this->simpleUser);
    // Simple authenticated user cannot access other's username.
    $this->assertUsernameWasRemoved($this->userWithViewPermission);

    $this->setCurrentUser($this->userWithViewPermission);
    // But the user with special permission can.
    $this->assertUsernameHasNotChanged($this->simpleUser);
  }

  /**
   * Assert if the current user can see the original username of user.
   *
   * @param \Drupal\Core\Session\AccountInterface $user
   *   The user.
   */
  abstract protected function assertUsernameHasNotChanged(AccountInterface $user): void;

  /**
   * Assert if the current user cannot see the username of user.
   *
   * @param \Drupal\Core\Session\AccountInterface $user
   *   The user.
   */
  abstract protected function assertUsernameWasRemoved(AccountInterface $user): void;

}
