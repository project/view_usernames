<?php

declare(strict_types = 1);

/**
 * Copyright (C) 2021 PRONOVIX GROUP.
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301,
 * USA.
 */

namespace Drupal\Tests\view_usernames\Kernel;

use Drupal\KernelTests\KernelTestBase;
use Drupal\Tests\user\Traits\UserCreationTrait;
use Drupal\user\RoleInterface;
use Drupal\view_usernames\EntityHooks;

/**
 * Tests if our handler only handles the expected operations, nothing more.
 */
final class EntityHooksBailOutTest extends KernelTestBase {

  use UserCreationTrait {
    createUser as drupalCreateUser;
  }

  /**
   * {@inheritdoc}
   */
  protected static $modules = [
    'system',
    'user',
    'view_usernames',
  ];

  /**
   * A simple registered user.
   *
   * @var \Drupal\user\UserInterface
   */
  private $simpleUser;

  /**
   * {@inheritdoc}
   */
  protected function setUp(): void {
    parent::setUp();

    $this->installSchema('system', 'sequences');
    $this->installEntitySchema('user');

    // Clear permissions for authenticated users.
    $this->config('user.role.' . RoleInterface::AUTHENTICATED_ID)
      ->set('permissions', [])
      ->save();

    // Reserve the user 1 who has special permissions by default.
    $this->drupalCreateUser([], 'UID1');

    $this->simpleUser = $this->drupalCreateUser([], 'simple user');
  }

  /**
   * Tests that only the view label operation handled on the user entity.
   */
  public function testOnlyUserViewLabelIsHandled(): void {
    /** @var \Drupal\view_usernames\EntityHooks $handler */
    $handler = $this->container->get('class_resolver')->getInstanceFromDefinition(EntityHooks::class);
    foreach (['view', 'update', 'delete'] as $operation) {
      self::assertTrue($handler->userEntityAccess($this->simpleUser, $operation, $this->simpleUser)->isNeutral(), "The {$operation} on the user entity is not handled by the handler.");
    }
  }

  /**
   * Tests that only the view operation handled and only on the username.
   */
  public function testOnlyUsernameFieldViewIsHandled(): void {
    /** @var \Drupal\view_usernames\EntityHooks $handler */
    $handler = $this->container->get('class_resolver')->getInstanceFromDefinition(EntityHooks::class);
    self::assertTrue($handler->entityFieldAccess('edit', $this->simpleUser->name->getFieldDefinition(), $this->simpleUser, $this->simpleUser->name)->isNeutral(), 'Edit operation on username is not handled by the handler.');
    foreach (['view', 'edit'] as $operation) {
      self::assertTrue($handler->entityFieldAccess($operation, $this->simpleUser->pass->getFieldDefinition(), $this->simpleUser, $this->simpleUser->pass)->isNeutral(), "The {$operation} on the password field is not handled by the handler.");
    }
  }

}
