<?php

declare(strict_types = 1);

/**
 * Copyright (C) 2022 PRONOVIX GROUP.
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301,
 * USA.
 */

namespace Drupal\view_usernames_test2;

use Drupal\Core\Cache\CacheableMetadata;
use Drupal\Core\Config\ConfigFactoryInterface;
use Drupal\view_usernames\Contracts\UserFormatNameHardeningBypasserInterface;
use Drupal\view_usernames\Type\Bypasser\BypassIt;
use Drupal\view_usernames\Type\Bypasser\BypasserResult;
use Drupal\view_usernames\Type\Bypasser\ExecuteIt;

/**
 * Dummy hardening bypasser that takes over the control.
 */
final class DummyUserFormatNameHardeningBypasser implements UserFormatNameHardeningBypasserInterface {

  public const CONFIG_NAME = 'view_usernames_test2.dummy_username_format_hardening_bypasser';

  /**
   * The config factory.
   *
   * @var \Drupal\Core\Config\ConfigFactoryInterface
   */
  private ConfigFactoryInterface $configFactory;

  /**
   * Constructs a new object.
   *
   * @param \Drupal\Core\Config\ConfigFactoryInterface $config_factory
   *   The config factory.
   */
  public function __construct(ConfigFactoryInterface $config_factory) {
    $this->configFactory = $config_factory;
  }

  /**
   * {@inheritdoc}
   */
  public function shouldByPass(): BypasserResult {
    $config = $this->configFactory->get(self::CONFIG_NAME);
    $active = $config->get('active');
    if ($active) {
      $result = BypassIt::create();
    }
    else {
      $result = ExecuteIt::create();
    }

    return $result->withCacheability(CacheableMetadata::createFromRenderArray([])->addCacheableDependency($config));
  }

}
