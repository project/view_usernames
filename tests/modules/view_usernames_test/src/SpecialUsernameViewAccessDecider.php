<?php

declare(strict_types = 1);

/**
 * Copyright (C) 2021 - 2023 PRONOVIX GROUP.
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301,
 * USA.
 */

namespace Drupal\view_usernames_test;

use Drupal\Core\Access\AccessResult;
use Drupal\Core\Access\AccessResultAllowed;
use Drupal\Core\Access\AccessResultForbidden;
use Drupal\Core\Session\AccountInterface;
use Drupal\user\UserInterface;
use Drupal\view_usernames\Contracts\ViewUsernameAccessDeciderInterface;

/**
 * Grants view username access to special accounts.
 */
final class SpecialUsernameViewAccessDecider implements ViewUsernameAccessDeciderInterface {

  public const SPECIAL_CACHE_CONTEXT_FOR_SPECIAL_ROLE = 'headers:' . self::class . ':foo';

  public const SPECIAL_USER_ROLE = 'content_editor';

  public const USERNAME_WITH_NO_PROTECTION = 'not_a_secret';

  public const SPECIAL_CACHE_CONTEXT_FOR_NOT_PROTECTED_USERNAME = 'headers:' . self::class . ':bar';

  /**
   * {@inheritdoc}
   */
  public function canViewUserName(AccountInterface $acting_user, UserInterface $other_user): AccessResultAllowed|AccessResultForbidden {
    $access = AccessResult::allowedIf($other_user->getAccountName() === self::USERNAME_WITH_NO_PROTECTION)->addCacheableDependency($other_user)
      ->addCacheContexts([self::SPECIAL_CACHE_CONTEXT_FOR_NOT_PROTECTED_USERNAME])
      ->orIf(
        AccessResult::allowedIf(in_array(self::SPECIAL_USER_ROLE, $other_user->getRoles(), TRUE))
          ->addCacheableDependency($other_user)
          ->addCacheContexts([self::SPECIAL_CACHE_CONTEXT_FOR_SPECIAL_ROLE])
      );

    // Convert neutral to explicit forbidden.
    if ($access->isNeutral()) {
      $access = $access->andIf(AccessResult::forbidden());
    }

    // @see https://github.com/mglaman/phpstan-drupal/issues/480
    assert($access instanceof AccessResultAllowed || $access instanceof AccessResultForbidden);

    return $access;
  }

}
