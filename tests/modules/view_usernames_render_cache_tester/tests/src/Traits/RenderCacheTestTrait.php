<?php

declare(strict_types = 1);

/**
 * Copyright (C) 2023 PRONOVIX GROUP.
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301,
 * USA.
 */

namespace Drupal\Tests\view_usernames_render_cache_tester\Traits;

use Drupal\Core\Session\AccountInterface;
use Drupal\Core\Url;

/**
 * Set of utility methods that render cache testers can leverage.
 */
trait RenderCacheTestTrait {

  /**
   * Visits the username listing page.
   */
  private function visitUsernameList(): void {
    $url = Url::fromRoute('view_usernames_render_cache_tester.username_list');
    $this->drupalGet($url->toString());
  }

  /**
   * Visits the username view page that uses getDisplayName() method call.
   *
   * @param \Drupal\Core\Session\AccountInterface $account
   *   An account's that username is viewed.
   */
  private function visitUsernameWithGetDisplayNamePage(AccountInterface $account): void {
    $url = Url::fromRoute('view_usernames_render_cache_tester.view_username_with_get_display_name', ['user' => $account->id()]);
    $this->drupalGet($url->toString());
  }

  /**
   * Visits the username view page that uses username theme.
   *
   * @param \Drupal\Core\Session\AccountInterface $account
   *   An account's that username is viewed.
   */
  private function visitUsernameWithUsernameThemePage(AccountInterface $account): void {
    $url = Url::fromRoute('view_usernames_render_cache_tester.view_username_with_username_theme', ['user' => $account->id()]);
    $this->drupalGet($url->toString());
  }

  /**
   * Visits the username view page of a user with another user.
   *
   * @param \Drupal\Core\Session\AccountInterface $account
   *   An account's that username is viewed.
   * @param \Drupal\Core\Session\AccountInterface $visitor
   *   A user who visits the $account's username page.
   */
  private function visitUsernameViewAsUser(AccountInterface $account, AccountInterface $visitor): void {
    $url = Url::fromRoute('view_usernames_render_cache_tester.view_username_as', [
      'user' => $account->id(),
      'view_as' => $visitor->id(),
    ]);
    $this->drupalGet($url->toString());
  }

  /**
   * Assert on different kind of view username pages.
   *
   * @param \Drupal\Core\Session\AccountInterface $user
   *   The user whose username view pages get visited.
   * @param callable ...$asserts
   *   List of callables that performs assertions on pages.
   */
  private function assertOnUsernameViewPages(AccountInterface $user, callable ...$asserts): void {
    $visits = [
      fn() => $this->visitUsernameWithGetDisplayNamePage($user),
      fn() => $this->visitUsernameWithUsernameThemePage($user),
    ];
    foreach ($visits as $visit) {
      $visit();
      foreach ($asserts as $assert) {
        $assert();
      }
    }
  }

}
