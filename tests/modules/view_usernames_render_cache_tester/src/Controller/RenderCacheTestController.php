<?php

declare(strict_types = 1);

/**
 * Copyright (C) 2021-2023 PRONOVIX GROUP.
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301,
 * USA.
 */

namespace Drupal\view_usernames_render_cache_tester\Controller;

use Drupal\Core\Cache\CacheableMetadata;
use Drupal\Core\Controller\ControllerBase;
use Drupal\Core\Entity\EntityTypeInterface;
use Drupal\Core\Session\AccountInterface;
use Drupal\user\UserInterface;
use Drupal\user\UserStorageInterface;
use Drupal\view_usernames\Contracts\ConfigurableUserFormatNameHardeningBypasserInterface;
use Drupal\view_usernames\Contracts\UserFormatNameHardeningBypasserInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Controller for testing render caching.
 */
final class RenderCacheTestController extends ControllerBase {

  /**
   * The user storage.
   *
   * @var \Drupal\user\UserStorageInterface
   */
  private UserStorageInterface $userStorage;

  /**
   * The user entity definition.
   *
   * @var \Drupal\Core\Entity\EntityTypeInterface
   */
  private EntityTypeInterface $userEntityType;

  /**
   * The hook_user_format_name_alter() hardening bypasser.
   *
   * @var \Drupal\view_usernames\Contracts\UserFormatNameHardeningBypasserInterface
   */
  private UserFormatNameHardeningBypasserInterface $userFormatNameHardeningBypasser;

  /**
   * Constructs a new object.
   *
   * @param \Drupal\user\UserStorageInterface $user_storage
   *   The user storage.
   * @param \Drupal\Core\Entity\EntityTypeInterface $user_def
   *   The user entity definition.
   * @param \Drupal\Core\Session\AccountInterface $current_user
   *   The current user.
   * @param \Drupal\view_usernames\Contracts\UserFormatNameHardeningBypasserInterface $user_format_name_hardening_bypasser
   *   The hook_user_format_name_alter() hardening bypasser.
   */
  public function __construct(UserStorageInterface $user_storage, EntityTypeInterface $user_def, AccountInterface $current_user, UserFormatNameHardeningBypasserInterface $user_format_name_hardening_bypasser) {
    $this->userStorage = $user_storage;
    $this->userEntityType = $user_def;
    $this->currentUser = $current_user;
    $this->userFormatNameHardeningBypasser = $user_format_name_hardening_bypasser;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container): self {
    return new self(
      $container->get('entity_type.manager')->getStorage('user'),
      $container->get('entity_type.manager')->getDefinition('user'),
      $container->get('current_user'),
      $container->get('view_usernames.user_format_name_hardening_bypasser')
    );
  }

  /**
   * Returns a list of usernames that the current user can view.
   *
   * @return array
   *   A render array.
   */
  public function listOtherUsers(): array {
    $build = [];
    $cacheability = CacheableMetadata::createFromRenderArray([]);
    $cacheability->addCacheTags($this->userEntityType->getListCacheTags());

    foreach ($this->userStorage->loadMultiple() as $user) {
      $build[] = [
        '#theme' => 'username',
        '#account' => $user,
        '#cache' => [
          'tags' => $user->getCacheTags(),
        ],
      ];
    }

    $cacheability->applyTo($build);
    return $build;
  }

  /**
   * View a user's username with getDisplayName() call as the current user.
   *
   * The access checking below happens by using the current user as acting user
   * because there is no way to pass another user to called getDisplayName()
   * method.
   *
   * @param \Drupal\user\UserInterface $user
   *   A user whose username to be viewed.
   *
   * @return array
   *   A render array.
   */
  public function viewUsernameWithGetDisplayName(UserInterface $user): array {
    $build = [];
    $cacheability = CacheableMetadata::createFromRenderArray([]);
    // Different acting user than the current user cannot be passed.
    $build[] = ['#markup' => $user->getDisplayName()];
    $cacheability->applyTo($build);
    return $build;
  }

  /**
   * View a user's username with "username" theme as the current user.
   *
   * The access checking below happens by using the current user as acting user
   * because there is no way to pass another user to called "username" theme.
   *
   * @param \Drupal\user\UserInterface $user
   *   A user.
   *
   * @return array
   *   A render array.
   */
  public function viewUsernameWithUsernameTheme(UserInterface $user): array {
    $build = [];
    $build[] = [
      '#theme' => 'username',
      '#account' => $user,
      '#cache' => [
        'tags' => $user->getCacheTags(),
      ],
    ];
    return $build;
  }

  /**
   * View a user's username as a user who may or may not be current user.
   *
   * @param \Drupal\user\UserInterface $user
   *   A user.
   * @param \Drupal\user\UserInterface $view_as
   *   A user who views the $user's username.
   *
   * @return array
   *   A render array.
   */
  public function viewUsernameAs(UserInterface $user, UserInterface $view_as): array {
    if ((int) $this->currentUser()->id() === (int) $view_as->id()) {
      throw new \LogicException('The acting user MUST NOT be the same as the current user otherwise the test might produce false-positive results.');
    }

    $build = [];
    $cacheability = CacheableMetadata::createFromRenderArray([]);
    $access = $user->access('view label', $view_as, TRUE);
    $cacheability->addCacheableDependency($access);
    // Because "view label" entity access is checked before
    // $user->getDisplayName() call therefore we can activate bypassing here.
    // For the sake of consistency relying on $user->getDisplayName() is
    // probably still better than using $user->getAccountName() instead that
    // does not call hook_user_format_name() implementations.
    if ($this->userFormatNameHardeningBypasser instanceof ConfigurableUserFormatNameHardeningBypasserInterface) {
      $this->userFormatNameHardeningBypasser->activate();
    }
    else {
      // Because this is a test (controller) class it is fine doing this sanity
      // check only here and not in the constructor.
      throw new \LogicException(sprintf('Cannot active bypassing on a non-configurable username hardening bypasser: %s.', get_class($this->userFormatNameHardeningBypasser)));
    }
    if ($access->isAllowed()) {
      $build[] = ['#markup' => $user->getDisplayName()];
    }

    $cacheability->applyTo($build);
    return $build;
  }

}
