<?php

/**
 * @file
 * Module file for View usernames.
 */

declare(strict_types = 1);

/**
 * Copyright (C) 2021-2022 PRONOVIX GROUP.
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301,
 * USA.
 */

use Drupal\Core\Access\AccessResultInterface;
use Drupal\Core\Cache\CacheableDependencyInterface;
use Drupal\Core\Cache\CacheableMetadata;
use Drupal\Core\Field\FieldDefinitionInterface;
use Drupal\Core\Field\FieldItemListInterface;
use Drupal\Core\Session\AccountInterface;
use Drupal\Core\Session\AnonymousUserSession;
use Drupal\user\UserInterface;
use Drupal\view_usernames\EntityHooks;
use Drupal\view_usernames\Utility\CacheabilityBubbleUpper;

/**
 * Implements hook_entity_access().
 */
function view_usernames_user_access(UserInterface $entity, string $operation, AccountInterface $account): AccessResultInterface {
  /** @var \Drupal\view_usernames\EntityHooks $handler */
  $handler = \Drupal::classResolver()->getInstanceFromDefinition(EntityHooks::class);
  return $handler->userEntityAccess($entity, $operation, $account);
}

/**
 * Implements hook_entity_field_access().
 *
 * @phpstan-param \Drupal\Core\Field\FieldItemListInterface<\Drupal\Core\Field\FieldItemInterface>|null $items
 */
function view_usernames_entity_field_access(string $operation, FieldDefinitionInterface $field_definition, AccountInterface $account, ?FieldItemListInterface $items = NULL): AccessResultInterface {
  /** @var \Drupal\view_usernames\EntityHooks $handler */
  $handler = \Drupal::classResolver()->getInstanceFromDefinition(EntityHooks::class);
  return $handler->entityFieldAccess($operation, $field_definition, $account, $items);
}

/**
 * Implements hook_preprocess_HOOK().
 */
function view_usernames_preprocess_username(array &$variables): void {
  // This is not supposed to be here but currently this is the safest way and
  // our last remedy to ensure every call that uses #theme => 'username'
  // inherits this access check. This is the best we can do for now.
  // Note: template_preprocess_username() calls for getDisplayName() and
  // getAccountName() on user.
  $account = $variables['account'] ?: new AnonymousUserSession();
  // (Anonymous' username can still always be viewed.)
  if (!$account->isAnonymous() && $account instanceof UserInterface) {
    $handler = \Drupal::entityTypeManager()->getAccessControlHandler('user');
    // Cacheability information has been already bubbled up at this point.
    // @see \view_usernames_user_format_name_alter()
    if ($handler->access($account, 'view label', NULL, TRUE)->isForbidden()) {
      $variables['name'] = '';
      $variables['name_raw'] = '';
      $variables['truncated'] = FALSE;
    }
  }
}

/**
 * Implements hook_module_implements_alter().
 */
function view_usernames_module_implements_alter(array &$implementations, string $hook): void {
  if ($hook === 'user_format_name_alter') {
    // (Try) to make sure our hook implementation runs the last.
    $group = $implementations['view_usernames'];
    unset($implementations['view_usernames']);
    $implementations['view_usernames'] = $group;
  }
}

/**
 * Implements hook_user_format_name_alter().
 */
function view_usernames_user_format_name_alter(string &$name, AccountInterface $account): void {
  // This is a last line of defense to protect against exposing usernames to
  // unprivileged users, entity view/view label access or username field view
  // access should have already eliminated this problem, but probably it was
  // not called by mistake.
  // This handles every direct call to
  // \Drupal\Core\Session\AccountInterface::getDisplayName().
  // Like the one in
  // \Drupal\user\Plugin\Field\FieldFormatter\UserNameFormatter::viewElements().
  $cacheabilty_bubbleupper = CacheabilityBubbleUpper::createWithDefaults();
  /** @var \Drupal\view_usernames\Contracts\UserFormatNameHardeningBypasserInterface $bypasser */
  $bypasser = \Drupal::service('view_usernames.user_format_name_hardening_bypasser');
  if ($bypasser->shouldByPass()->getCacheability()) {
    $cacheabilty_bubbleupper->bubbleUpIfPossible($bypasser->shouldByPass()->getCacheability());
  }
  // The decision by design is not per $account instead of a generic one. When
  // per user based decision is needed then possibly a decider should be
  // implemented that also affects entity access.
  // (If we later realize that we need per user based bypass then PHP allows
  // as to change the interface definition with
  // "/* AccountInterface $account */" parameter without breaking BC.)
  if ($bypasser->shouldByPass()->asBool()) {
    return;
  }
  // If an $account is not a (persisted) user entity, possibly there is nothing
  // to protect on its identity. Assumption: it can be anonymous or a stub
  // entity.
  if ($account instanceof UserInterface) {
    $handler = \Drupal::entityTypeManager()->getAccessControlHandler('user');
    /** @var \Drupal\Core\Access\AccessResultInterface $access */
    $access = $handler->access($account, 'view label', NULL, TRUE);
    // Make sure the cacheability information from access result
    // bubble ups even if the display name is printed directly.
    if ($access instanceof CacheableDependencyInterface) {
      $cacheabilty_bubbleupper->bubbleUpIfPossible($access);
    }
    else {
      $cacheabilty_bubbleupper->bubbleUpIfPossible(CacheableMetadata::createFromRenderArray(['#cache' => ['max-age' => 0]]));
    }
    if ($access->isForbidden()) {
      $name = '';
    }
  }
}
